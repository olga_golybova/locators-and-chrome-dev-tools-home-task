package com.ta.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends BasePage{

    @FindBy(xpath = "//button[@class='button primary compose']")
    private WebElement createNewEmail;

    public MainPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    public MailPage invokeNewEmailCreation()
    {
        this.waitVisibilityOfElement(50,createNewEmail);
        createNewEmail.click();
        return new MailPage(driver);
    }
}
