package com.ta.page;

import com.ta.model.User;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {
    private final String PAGE_URL = "https://mail.ukr.net/";

    @FindBy(name = "login")
    private WebElement inputEmail;

    @FindBy(name = "password")
    private WebElement inputPassword;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement buttonSubmit;

    public LoginPage(WebDriver driver)
    {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    public LoginPage openPage()
    {
        driver.navigate().to(PAGE_URL);
        return this;
    }

    public MainPage login(User user)
    {
        this.waitVisibilityOfElement(100, inputEmail);
        inputEmail.sendKeys(user.getUsername() + Keys.ENTER);
        inputPassword.sendKeys(user.getPassword() + Keys.ENTER);
        buttonSubmit.click();

        return new MainPage(driver);
    }



}
