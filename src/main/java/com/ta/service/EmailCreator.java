package com.ta.service;

import com.ta.model.Email;

public class EmailCreator {
    public static final String TESTDATA_EMAIL_RECEPIENT = "testdata.email.recipient";
    public static final String TESTDATA_EMAIL_TOPIC = "testdata.email.topic";
    public static final String TESTDATA_EMAIL_TEXT = "testdata.email.text";

    public static Email withDataFromProperty(){
        return new Email(TestDataReader.getTestData(TESTDATA_EMAIL_RECEPIENT),
                TestDataReader.getTestData(TESTDATA_EMAIL_TOPIC),
                TestDataReader.getTestData(TESTDATA_EMAIL_TEXT));
    }
}
