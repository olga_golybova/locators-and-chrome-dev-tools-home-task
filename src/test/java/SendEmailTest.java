import com.ta.model.Email;
import com.ta.model.User;
import com.ta.page.LoginPage;
import com.ta.service.EmailCreator;
import com.ta.service.UserCreator;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SendEmailTest extends CommonConditions{

    @Test
    public void sendNewEmailTest()
    {
        User testUser = UserCreator.withCredentialsFromProperty();
        Email testEmail = EmailCreator.withDataFromProperty();

        boolean sendEmail = new LoginPage(driver)
                .openPage()
                .login(testUser)
                .invokeNewEmailCreation()
                .sendMail(testEmail)
                .isEmailSent();

        assertTrue("mail didn't send", sendEmail);
    }

}
