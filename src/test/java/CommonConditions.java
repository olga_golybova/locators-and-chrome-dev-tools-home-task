import com.ta.driver.Driver;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

public class CommonConditions {
    protected WebDriver driver;

    @Before()
    public void setUp()
    {
        driver = Driver.getDriver();
    }

    @After
    public void stopBrowser()
    {
        Driver.closeDriver();
    }
}
